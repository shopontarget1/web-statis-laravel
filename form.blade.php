<!DOCTYPE html>
<html>
<head>
	<title>Form Sign Up</title>
</head>
<body>
	<h2>Buat Account Baru!</h2>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="POST">
		<label>First Name</label> <br><br>
		<input type="text" name="first_name"><br><br>

		<label>Last Name</label><br><br>
		<input type="text" name="last_name"><br><br>

		<label>Gender</label> <br>
		<input type="radio" name="G"> Male <br>
		<input type="radio" name="G"> Female <br>
		<input type="radio" name="G"> Other <br><br>

		<label>Nationality:</label> <br><br>
		<select name="Nationality"> <br>
			<option value="Indonesian">Indonesian</option> <br>
			<option value="English">English</option>
			<option value="Malaysian">Malaysian</option>
		</select> <br><br>

		<label>Language Spoken:</label> <br><br>
		<input type="checkbox" name="">Bahasa Indonesia <br>
		<input type="checkbox" name="">Bahasa Inggris <br>
		<input type="checkbox" name="">Other <br><br>

		<label>Bio:</label> <br><br>
		<textarea name="textarea" cols="40" rows="20"></textarea> <br>

		<input type="submit" value="Sign Up">
	</form>


</body>
</html>